//
//  Constants.swift
//  NYTIMES
//
//  Created by Admin on 9/2/19.
//  Copyright © 2019 eurisko. All rights reserved.
//

import Foundation
import UIKit

//******************** Public Usage ********************//
let appDelegate: AppDelegate? = {
    guard let _appDelegate = UIApplication.shared.delegate as? AppDelegate else {
        return nil
    }
    return _appDelegate
}()

enum CellIdentifiers: String {
    case NewsTableViewCell
}

enum DaysKeys: String {
    case thirtyDays = "30"
    case sevenDays = "7"
    case oneDay = "1"
}

enum SectionKeys: String {
    case all = "all-sections"
}

enum ViewIdentifiers: String {
    case HeaderView
}

enum ViewControllerIdentifiers: String {
    case NewsDetailViewController
}

var coreapi = APIConfig.shared
var APIKey = "zqzjIG7UiTWY0snKcGIQxW4G6dgqhD2O"

//******************** End of Public Usage ********************//
