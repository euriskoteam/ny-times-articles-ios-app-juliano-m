//
//  Globals.swift
//  NYTIMES
//
//  Created by Admin on 9/2/19.
//  Copyright © 2019 eurisko. All rights reserved.
//

import Foundation
import UIKit

let activityIndicator:UIActivityIndicatorView = UIActivityIndicatorView()

//MARK:- Setup Loader
func showloading(vc: UIViewController){
    DispatchQueue.main.async {
        activityIndicator.center = vc.view.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.style = UIActivityIndicatorView.Style.gray
        vc.view.addSubview(activityIndicator)
        
        activityIndicator.startAnimating()
        UIApplication.shared.beginIgnoringInteractionEvents()
    }
}

func hideloading(){
    DispatchQueue.main.async {
        activityIndicator.stopAnimating()
        UIApplication.shared.endIgnoringInteractionEvents()
    }
}

//MARK:- Show Alert
func alert(vc: UIViewController, title: String, message:String){
    let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
    let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
    alert.addAction(okAction)
    vc.present(alert,animated: true, completion: nil)
}

