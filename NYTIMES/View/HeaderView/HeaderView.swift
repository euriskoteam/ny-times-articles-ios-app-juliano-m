//
//  HeaderView.swift
//  NYTIMES
//
//  Created by Admin on 9/2/19.
//  Copyright © 2019 eurisko. All rights reserved.
//

import UIKit

//Mark:- HeaderView Buttons Delegation
protocol HeaderViewDelegate {
    func onMenuPress()
    func onSearchPress()
    func onMoreOptionPress()
}

class HeaderView: UIView {

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var sideMenuButton: UIButton!
    @IBOutlet weak var settingsButton: UIButton!
    
    var delegate: HeaderViewDelegate?
    
    @IBAction func menuPress(_ sender: Any) {
        delegate?.onMenuPress()
    }
    
    @IBAction func searchPress(_ sender: Any) {
        delegate?.onSearchPress()
    }
    
    @IBAction func moreOptionPress(_ sender: Any) {
        delegate?.onMoreOptionPress()
    }
    
}
