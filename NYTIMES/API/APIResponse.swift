
import Foundation

// MARK:- API Response Status
enum ResponseStatus: Int {
    /* CHANGE IT TO EXACT CODE USED IN YOUR PROJECT */
    case success = 1
    case failure = 0
    case conenctionTimeout = -1
}

// MARK:- API Response Message
struct ResponseMessage {
    static let serverUnreachable: String = "An error has occured"
    static let connectionTimeout: String = "Please check your internet connection"
}

// MARK:- API Response Data
struct ResponseData {
    var status:ResponseStatus?
    var message: String = String()
    var data: Data? = nil
    
    init() {}
}
