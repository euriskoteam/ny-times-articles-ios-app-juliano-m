
import Foundation
import Alamofire

//MARK:- API HTTP Parameters
typealias Parameters = [String: Any]

//MARK:- API HTTP Headers
typealias HTTPHeaders = [String: Any]

//MARK:- API CompletionHandler
typealias CompletionHandler = (_ response: ResponseData) -> Void

//MARK:- API Manager
struct APIManager {
    
    // MARK API Manager Shared Instance
    static let shared = APIManager.init()
    
    // MARK API Private Init
    private init() {}
    
    //MARK:- API SERVER REQUEST
    func makeHttpRequest(endPoint: String, parameters: Parameters? = nil, headers: HTTPHeaders? = nil, completionHandler: @escaping CompletionHandler) {
        
        var responseData = ResponseData()
        let myEndpoint = endPoint.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        
        //Check URL
        guard let url = URL(string: "\(coreapi.BASE_URL)\(myEndpoint ?? "")") else {
            responseData.status = .failure
            responseData.message = ResponseMessage.serverUnreachable
            completionHandler(responseData)
            return
        }
        
        Alamofire.request(url,
                          method: .get,
                          parameters: parameters)
            .validate()
            .responseJSON { response in
                
                guard response.result.isSuccess else {
                    responseData.status = .failure
                    responseData.message = ""
                    responseData.data = response.data
                    completionHandler(responseData)
                    return
                }
                
                responseData.status = .success
                responseData.message = ""
                responseData.data = response.data
                completionHandler(responseData)
                
        }
    }
}
