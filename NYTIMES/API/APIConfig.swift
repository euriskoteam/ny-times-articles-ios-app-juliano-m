
import Foundation

struct AppEnvironment {
    static let API_ENDPOINT = "http://api.nytimes.com/svc/mostpopular/v2"
}

struct APIConfig {
    
    //MARK:- API Config Shared Instance
    static let shared = APIConfig.init()
    private init() {}
    
    //MARK:- API BaseUrl
    var BASE_URL: String {
        return AppEnvironment.API_ENDPOINT
    }
    
}

//MARK:- API HTTP Request Content Type
struct ContentType {
    static let json = "application/json"
    static let formURLEncoded = "application/x-www-form-urlencoded"
}

//MARK:- API HTTP Request Methods
enum HTTPMethod: String {
    case delete = "DELETE"
    case get = "GET"
    case post = "POST"
    case put = "PUT"
}
