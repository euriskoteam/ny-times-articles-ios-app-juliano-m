//
//  NewsDetailViewController.swift
//  NYTIMES
//
//  Created by Admin on 9/2/19.
//  Copyright © 2019 eurisko. All rights reserved.
//

import UIKit
import Kingfisher

class NewsDetailViewController: UIViewController {

    @IBOutlet weak var newsImage: UIImageView!
    @IBOutlet weak var newsDesc: UILabel!
    @IBOutlet weak var newsSubDesc: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var newsAuthor: UILabel!
    @IBOutlet weak var imageHeightConstraint: NSLayoutConstraint!
    
    var selectedItem: Results!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigationBar()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    
    func setupViews() {
        loadImage()
        newsDesc.text = selectedItem.title
        newsSubDesc.text = selectedItem.abstract
        newsAuthor.text = selectedItem.byline
        if let date = selectedItem.published_date {
            dateLabel.text = "On " + date
        } else {
            dateLabel.text = ""
        }
    }
    
    func loadImage() {
        newsImage.kf.indicatorType = .activity
        guard let image = getImageWithFormat("Large"),
            let width = image.width,
            let height = image.height,
            let url = image.url else {
                return
        }
        let imageWidth = Int(UIScreen.main.bounds.width) - (8 + 8) // margins
        let imageHeight = imageWidth * height / width
        imageHeightConstraint.constant = CGFloat(imageHeight)
        newsImage.kf.setImage(with: URL(string: url))
    }
    
    func getImageWithFormat(_ format: String) -> MediaMetaData? {
        guard let mediaMetadata = selectedItem.media?.first?.media_metadata?
            .filter({ (metadata) -> Bool in
                return metadata.format == format
            }).first else { return nil }
        return mediaMetadata
    }
    
    func setupNavigationBar() {
        navigationController?.setNavigationBarHidden(false, animated: true)
        navigationController?.navigationBar.barTintColor = UIColor.init(red: 80/255, green: 227/255, blue: 194/255, alpha: 1)
        
        navigationController?.navigationBar.tintColor = .white
    }

}
