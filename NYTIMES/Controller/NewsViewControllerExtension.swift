//
//  NewsViewControllerExtension.swift
//  NYTIMES
//
//  Created by Admin on 9/3/19.
//  Copyright © 2019 eurisko. All rights reserved.
//

import UIKit

extension NewsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let newsDetailVC = storyboard?.instantiateViewController(withIdentifier: ViewControllerIdentifiers.NewsDetailViewController.rawValue) as? NewsDetailViewController else {
            return
        }
        newsDetailVC.selectedItem = tableData[indexPath.row]
        navigationController?.pushViewController(newsDetailVC, animated: true)
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension NewsViewController: HeaderViewDelegate {
    func onMenuPress() {}
    
    func onSearchPress() {}
    
    func onMoreOptionPress() {}
}

    
extension NewsViewController: UITableViewDataSource {
    
    //Mark:- TableViewDataSource
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tableData.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.NewsTableViewCell.rawValue) as? NewsTableViewCell else {
            return UITableViewCell()
        }
        cell.accessoryType = .disclosureIndicator
        cell.backgroundColor = .clear
        cell.tintColor = .red
        
        let result = self.tableData[indexPath.row]
        
        cell.newsTitle.text = result.title
        cell.newsDescription.text = result.byline
        
        if let imageURL = result.media?.first?.media_metadata?.first?.url {
            cell.newsImage?.kf.setImage(with: URL(string: imageURL))
        }
        
        
        DispatchQueue.main.async {
            cell.newsImage.layer.cornerRadius = 45 / 2
        }
        
        return cell
    }
}
