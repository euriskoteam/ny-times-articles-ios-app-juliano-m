//
//  NewsViewController.swift
//  NYTIMES
//
//  Created by Admin on 9/2/19.
//  Copyright © 2019 eurisko. All rights reserved.
//

import UIKit
import CoreData

class NewsViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewTopConstraint: NSLayoutConstraint!
    
    var tableData: [Results] = []

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.accessibilityIdentifier = "NewsViewController"

        fetchNews()
        setupHeaderView()
        setupTableView()
    }

    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationItem.title = ""
    }
    
    //Mark:- Fetching News API
    func fetchNews() {
        let managedContext = appDelegate?.persistentContainer.viewContext
        let newsEntity = NSEntityDescription.entity(forEntityName: "SavedNews", in: managedContext!)!
        let newsData = NSManagedObject(entity: newsEntity, insertInto: managedContext)
        
        if (retrieveData().count > 0){
            decodeNewsData(data: retrieveData())
        }
        
        showloading(vc: self)
        APIManager.shared.makeHttpRequest(endPoint: APIEndPoints.fetchNews, parameters: nil, headers: nil) { (ResponseData) in
            
            hideloading()

            if let data = ResponseData.data {
                if ResponseData.status == ResponseStatus.success {
                    newsData.setValue(data, forKey: "newsdata")
                    
                    do {
                        try managedContext?.save()
                    } catch let error as NSError {
                        print("Could not save News. \(error)")
                    }
                    
                    self.decodeNewsData(data: data)
                }
            }
        }
    }
    
    //MARK:- Decode news Data
    func decodeNewsData(data: Data) {
        let jsonDecoder = JSONDecoder()

        do {
            let news = try jsonDecoder.decode(News.self, from: data)
            guard let results = news.results else {
                alert(vc: self, title: "Something went wrong, please try again later.", message: "")
                return
            }
            self.tableData = results
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        } catch {
            alert(vc: self, title: "Error!", message: "Something went wrong, please try Again later")
        }
    }
    
    //MARK:- Header View frame setup
    func setupHeaderView() {
        if let headerView = Bundle.main.loadNibNamed(ViewIdentifiers.HeaderView.rawValue, owner: nil, options: nil)?.first as? HeaderView {
            if let statusBarHeight = UIApplication.shared.windows.last?.safeAreaInsets.top {
                headerView.frame.size = CGSize(width: view.frame.width, height: headerView.frame.height + statusBarHeight)
            }
            view.addSubview(headerView)
            headerView.delegate = self
            
            tableViewTopConstraint.constant = headerView.frame.height
        }
    }
    
    func setupTableView() {
        tableView.register(UINib(nibName: CellIdentifiers.NewsTableViewCell.rawValue, bundle: nil), forCellReuseIdentifier: CellIdentifiers.NewsTableViewCell.rawValue)
        tableView.tableFooterView = UIView()
    }
    
    //MARK:- Retrieve Data from CoreData Database
    func retrieveData() -> Data {
        let managedContext = appDelegate?.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "SavedNews")
        do {
            let result = try managedContext?.fetch(fetchRequest)
            for data in result as! [NSManagedObject] {
                if (data.value(forKeyPath: "newsdata") != nil) {
                    return data.value(forKey: "newsdata") as! Data
                }
            }
        } catch {
            print("Failed")
        }
        
        return Data()
    }
}
