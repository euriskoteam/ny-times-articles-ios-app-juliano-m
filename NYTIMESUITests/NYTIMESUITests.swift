//
//  NYTIMESUITests.swift
//  NYTIMESUITests
//
//  Created by Admin on 9/2/19.
//  Copyright © 2019 eurisko. All rights reserved.
//

import XCTest

class NYTIMESUITests: XCTestCase {
    
    var app: XCUIApplication!


    override func setUp() {
        continueAfterFailure = false
        app = XCUIApplication()
        app.launch()
        
        app.launchArguments.append("--uitesting")
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testMenuButton() {
        XCTAssertTrue(app.isDisplayingNewsViewController)

        app.tables/*@START_MENU_TOKEN@*/.staticTexts["By ANNIE KARNI and MAGGIE HABERMAN"]/*[[".cells.staticTexts[\"By ANNIE KARNI and MAGGIE HABERMAN\"]",".staticTexts[\"By ANNIE KARNI and MAGGIE HABERMAN\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        XCTAssertTrue(app.navigationBars["NYTIMES.NewsDetailView"].exists)
        
        // NewsViewController should no longer be displayed
        XCTAssertFalse(!app.isDisplayingNewsViewController)
        
        app.navigationBars["NYTIMES.NewsDetailView"].buttons["Back"].tap()
        XCTAssertTrue(app.isDisplayingNewsViewController)
    }
}

extension XCUIApplication {
    var isDisplayingNewsViewController: Bool {
        return otherElements["NewsViewController"].exists
    }
}
