//
//  NYTIMESTests.swift
//  NYTIMESTests
//
//  Created by Admin on 9/2/19.
//  Copyright © 2019 eurisko. All rights reserved.
//

import XCTest
@testable import NYTIMES

class NYTIMESTests: XCTestCase {
    
    var News: NewsViewController!


    override func setUp() {
        News = NewsViewController()
    }

    override func tearDown() {
        News = nil
    }
    
    func testFetchNewsAPI() {
        
        let promise = expectation(description: "Status code: 200")

        APIManager.shared.makeHttpRequest(endPoint: APIEndPoints.fetchNews, parameters: nil, headers: nil) { (ResponseData) in
            if let statusCode = (ResponseData.status) {
                if statusCode == ResponseStatus.success {
                    promise.fulfill()
                } else {
                    XCTFail("Status code: \(statusCode)")
                }
            }
        }
        wait(for: [promise], timeout: 5)
    }
}
