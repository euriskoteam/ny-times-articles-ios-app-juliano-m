
## NYTimes iOS App

# This app is using the below pods as dependencies
  - ReachabilitySwift: For networking changing detection.
  - Kingfisher: pure-Swift library for downloading and caching images from the web.
  - SwiftDate: Easy & pure Swift library to handle all dates conversion.
  - Alamofire: Alamofire is an HTTP networking library written in Swift.

# Main features
  - White themed app listing the mostpopular & mostviewed news.
  - fetch news data source.
  - Offline data caching so you can read the news anytime you want.
  
You can also:
  - Change the news source by going to 'APIEndPoints.swift'
  - Find "fetchNews(section: .all, period: .thirtyDays)"
  - Change the 'section:' and/or 'period:' enums to get data as your needs
  
# Automation & UniTests
- This app is using fastlane command lines for CD/CI
- The sever will prepare the app for release with app metadata within the 'fastlane' folder.
- To Run Unit Test: " $cd project directory  $fastlane tests" 
